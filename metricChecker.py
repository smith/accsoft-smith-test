#!/usr/bin/python
import os
import argparse
import socket
import time

execfile("test-utils.py")

class MetricCheckerTest(MetricChecker):
    def __init__(self, arguments):
        MetricChecker.__init__(self)
        self.arguments = arguments
        logFile = "%s/%s_%d.log"%(self.arguments['logpath'],socket.gethostname(), os.getpid())
        print("Stress test: host: %s  -   PID: %d   -   log: %s"%(self.arguments["hostname"], os.getpid(), logFile))
        logging.basicConfig(filename=logFile, level=logging.INFO)
        
    def performMetricCheck(self, fileMetrics):
        params = {'component': self.arguments['component']}
        url = 'http://' + self.arguments["hostname"] + '/cmx'
        try:
            remoteMetrics = self.getRemoteMetrics(url, params)
        except requests.exceptions.RequestException as e:
            self.logError(logging, e)
            return
            
        try:
            self.checkMetricsConsistency(fileMetrics, remoteMetrics, logging)
            self.logInfo(logging, "Stress test phase executed successfully")
        except:
             pass
               
    def testMultiDataConsistency(self):
        fileMetrics = self.getMetricsFromFile(self.arguments['metricspath']);
        while True:
            time.sleep(float(self.arguments['frequency']))
            self.performMetricCheck(fileMetrics)
    
    def __del__(self):
        logging.shutdown()

def main(argv=None):
     # argument parser
    parser = argparse.ArgumentParser(description="SMITH Agent Stress Test")
    parser.add_argument("-n", "--hostname",   action="store", dest="hostname", required=True,  help="Hostname to connect with")
    parser.add_argument("-a", "--component",  action="store", dest="component", required=True,  help="CMX Component to scrape")
    parser.add_argument("-f", "--frequency",  action="store", dest="frequency", required=True,  help="Frequency to scrape")
    parser.add_argument("-l", "--logpath",    action="store", dest="logpath", default="/tmp",   help="Path where the log will be saved")
    parser.add_argument("-m", "--metricspath",action="store", dest="metricspath", default="resources/metrics.dat",   help="Metrics to be compared with the remote ones")
    
    args = parser.parse_args()
    arguments = vars(args)
       
    metricCheckerTest = MetricCheckerTest(arguments)
    metricCheckerTest.testMultiDataConsistency()
    
if __name__ == "__main__":
    main()
