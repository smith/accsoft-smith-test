import unittest
import time
import logging

execfile("test-utils.py")
 
class IntegrationTestCase(AgentTestCase, MetricChecker):
    def setUp(self):
        AgentTestCase.setUp(self)
        MetricChecker.__init__(self)
        self.counterMetrics = dict()
        logging.basicConfig(level=logging.INFO)
        
    def dataConsistency(self, fileMetrics):
        params = {'component': 'another'}
        url = 'http://cfv-774-cgfes1/cmx'
        remoteMetrics = self.getRemoteMetrics(url, params)
    
        self.assertTrue(self.checkMetricsConsistency(fileMetrics, remoteMetrics, logging))
             
    def dataInconsistency(self, fileMetrics):
        params = {'component': 'another'}
        url = 'http://cfv-774-cgfes1/cmx'
        remoteMetrics = self.getRemoteMetrics(url, params)
        
        with self.assertRaises(Exception):
             self.checkMetricsConsistency(fileMetrics, remoteMetrics, logging)            
              
    def testMultiDataConsistencyPass(self):
        fileMetrics = self.getMetricsFromFile(resourcesTestRootDir + "another.dat");
        for index in range(0,100):
            self.dataConsistency(fileMetrics)
            
    def testMultiDataConsistencyFail(self):
        fileMetrics = self.getMetricsFromFile(resourcesTestRootDir + "wrong.dat");
        for index in range(0,100):
            self.dataInconsistency(fileMetrics)
            
    def __del__(self):
        logging.shutdown()