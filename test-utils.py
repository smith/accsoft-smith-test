import unittest
import tempfile
import requests
import os
import shutil
import re
import logging

resourcesTestRootDir = os.path.dirname(__file__) + "/resources/" 

class AgentTestCase(unittest.TestCase):
    def setUp(self):
        self.dir = tempfile.mkdtemp()
        os.chdir(self.dir)
        
    def tearDown(self):
        shutil.rmtree(self.dir)  

class MetricChecker:
    def __init__(self):
        self.counterMetrics = dict()

        # Return [metric, component, value]
    def parseData(self, rawdata):
        rawMetrics = rawdata.splitlines(True)
        metrics = list()
        for metric in rawMetrics:
            metric = metric.strip()
            metrics.append(re.split("{component=\"(.*)\"} ", metric))
            metrics[-1][2] = float(metrics[-1][2])        
        
        return metrics
      
    def checkMetricsConsistency(self, fileMetrics, remoteMetrics, logger):
        error = False
        exceptionMsg = ""
        if len(fileMetrics) != len(remoteMetrics):
            error = True
            exceptionMsg = "Remote and local data contain different number of metrics."
            self.logError(logger, exceptionMsg)          
        else:
            for index, remoteMetric in enumerate(remoteMetrics):
                if remoteMetric[0].find("_counter") != -1:
                    if remoteMetric[0] in self.counterMetrics:
                        if self.counterMetrics[remoteMetric[0]] > remoteMetric[2]:
                            msg = "Metric counter '%s' decreased its value instead of increase it. (%f > %f)"%(remoteMetric[0],self.counterMetrics[remoteMetric[0]], remoteMetric[2])
                            self.logError(logger, msg)
                            exceptionMsg += msg + "\n"
                            error = True
                    self.counterMetrics[remoteMetric[0]] = remoteMetric[2];
                elif remoteMetric[0].find("_random") == -1 and fileMetrics[index][2] != remoteMetric[2]:
                        msg = "Remote metric '%s' value and local metric value are different. (%f != %f)"%(remoteMetric[0],fileMetrics[index][2], remoteMetric[2])
                        self.logError(logger, msg)
                        exceptionMsg += msg + "\n"
                        error = True   
    
                if fileMetrics[index][0] != remoteMetric[0]:
                    msg = "Remote metric name and local metric name are different. (%s != %s)"%(fileMetrics[index][0], remoteMetric[0])
                    self.logError(logger, msg)
                    exceptionMsg += msg + "\n"
                    error = True
                    
                if fileMetrics[index][1] != remoteMetric[1]:
                    msg = "Remote component name and local component name are different. (%s != %s)"%(fileMetrics[index][1], remoteMetric[1])
                    self.logError(logger, msg)
                    exceptionMsg += msg + "\n"
                    error = True
        
        if error == True:
            raise Exception(exceptionMsg)
              
        return True
    
    def getRemoteMetrics(self, url, params):
        rawdata = requests.get(url, params=params)
        return self.parseData(rawdata.text)
    
    def getMetricsFromFile(self, file):
        file = open(file, "r")
        fileMetrics = self.parseData(file.read()) 
        file.close()
        return fileMetrics
      
    def logError(self, logger, msg):   
         logMsg = "%s%s" %(self.logHeader(), msg)
         logger.error(logMsg)
         
    def logInfo(self, logger, msg):   
         logMsg = "%s%s" %(self.logHeader(), msg)
         logger.info(logMsg)
         
    def logHeader(self):
        time_ = time.strftime("%Y/%m/%d-%H:%M:%S")
        return "[%s][PID:%s] " %(time_, self.pid)
